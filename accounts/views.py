from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User

# Create your views here.

    # You need to create a function view to handle showing the sign-up form, and handle its submission. This is a view, so you should create it in the file in the accounts directory that should hold the views.

def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)



# def signup(request):
#     if request.method == 'post':
#         # print(request.method)
#         # print(request.post)
#         formvariable = UserCreationForm(request.POST)
#         if formvariable.is_valid():
#             user = formvariable.save
#             login(request, user)
#             return redirect("home")

#     else:
#         formvariable = UserCreationForm()

#     return render(request, 'registration/signup.html', {"form": formvariable})
