from django.urls import path
from receipts.views import (ReceiptListView, ReceiptCreateView)

# receipts/
urlpatterns = [
    path('', ReceiptListView.as_view(), name='home'),
    path('create/', ReceiptCreateView.as_view(), name='create_receipt')

]
