from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView

from receipts.models import Receipt

class ReceiptListView(LoginRequiredMixin, ListView):
    model=Receipt
    template_name="receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

        # Change the queryset of the view to filter the Receipt objects where purchaser equals the logged in user.

class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name="receipts/create.html"
    fields=['vendor', 'total', 'tax', 'date','category', 'account']

# removed 'date' from fields above

    def form_valid(self, form):
        item = form.save(commit=False)
        item.purchaser = self.request.user
        item.save()
        form.save_m2m
        return redirect('home')

        # Setting the user
        # If you are using class views, you can set the user by writing your own form_valid method. The general form of a method that assigns the current user to a User property looks like this.